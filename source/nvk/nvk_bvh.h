/*
 * Copyright © 2023 Valve Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef NVK_BVH_H
#define NVK_BVH_H

#include <stdint.h>

/* Pretty sure this is 100% sideband data... */
struct nvk_bvh_header {
   uint32_t unknown0;
   uint32_t unknown1;
   uint32_t bvh_size;
   uint32_t unknown3;
   uint32_t leaf_nodes_offset;
   uint32_t unknown5;
   uint32_t unknown6;
   uint32_t unknown7;
   uint32_t internal_nodes_offset;
   uint32_t unknown9;
   uint32_t internal_metadata_offset;
   uint32_t unknown11;
   uint32_t unknown12;
   uint32_t unknown13;
   uint32_t unknown14;
   uint32_t instance_nodes_offset;
   uint32_t unknown16;
   uint32_t unknown17;
   uint32_t float_list_offset;
   uint32_t unknown19;
   uint32_t unknown20;
   uint32_t unknown21;
   uint32_t unknown22;
   uint32_t unknown23;
   uint32_t unknown24;
   uint32_t unknown25;
   uint32_t unknown26;
   uint32_t unknown27;
   uint32_t unknown28;
   uint32_t primitive_count;
   uint32_t unknown30;
   uint32_t unknown31;
   uint32_t unknown32;
   uint32_t unknown33;
   uint32_t unknown34;
   uint32_t unknown35;
   uint32_t unknown36;
   uint32_t unknown37;
   uint32_t unknown38;
   uint32_t unknown39;
   uint32_t unknown40;
   uint32_t unknown41;
   uint32_t unknown42;
   uint32_t unknown43;
   uint32_t unknown44;
   uint32_t unknown45;
   uint32_t unknown46;
   uint32_t unknown47;
   uint32_t unknown48;
   uint32_t unknown49;
   uint32_t unknown50;
   uint32_t unknown51;
   uint32_t unknown52;
   uint32_t unknown53;
   uint32_t unknown54;
   uint32_t unknown55;
   uint32_t unknown56;
   uint32_t unknown57;
   uint32_t unknown58;
   uint32_t unknown59;
   uint32_t unknown60;
   uint32_t unknown61;
   uint32_t unknown62;
   uint32_t unknown63;
   uint32_t unknown64;
   uint32_t unknown65;
   uint32_t unknown66;
   uint32_t unknown67;
   uint32_t unknown68;
   uint32_t unknown69;
   uint32_t unknown70;
   uint32_t unknown71;
   uint32_t unknown72;
   uint32_t unknown73;
   uint32_t unknown74;
   uint32_t unknown75;
   uint32_t unknown76;
   uint32_t unknown77;
   uint32_t unknown78;
   uint32_t unknown79;
   uint32_t unknown80;
   uint32_t unknown81;
   uint32_t unknown82;
   uint32_t unknown83;
   uint32_t unknown84;
   uint32_t unknown85;
   uint32_t unknown86;
   uint32_t unknown87;
   uint32_t unknown88;
   uint32_t unknown89;
   uint32_t unknown90;
   uint32_t unknown91;
   uint32_t unknown92;
   uint32_t unknown93;
   uint32_t unknown94;
   uint32_t unknown95;
};

struct nvk_vec3 {
   float x;
   float y;
   float z;

#ifdef __cplusplus
   nvk_vec3(float x, float y, float z): x(x), y(y), z(z)
   {
   }

   nvk_vec3(): x(0.0), y(0.0), z(0.0)
   {
   }
#endif
};

enum nvk_node_type {
   nvk_node_internal,
   nvk_node_instance,
   nvk_node_triangle,
};

#define NVK_MAX_CHILDREN_PER_NODE 12

struct nvk_internal_child {
   uint8_t min_x;
   uint8_t max_x;
   uint8_t min_y;
   uint8_t max_y;
   uint8_t min_z;
   uint8_t max_z;
   uint8_t cull_mask;
   /*
    * bits: usage
    * 0-5 : Unknown, used for indexing into the child node.
    * 6-7 : nvk_node_type
    */
   uint8_t packed_data;
};

struct nvk_internal_node {
   uint8_t unknown0; // triangles: 0x60 aabbs: 0x0 instances: 0xa0
   uint8_t grid_size_exp[3];
   struct nvk_vec3 min;
   int32_t relative_leaf_offset;
   /* bits : usage
    * 0 -23: Unknown, always 0x0.
    * 24-27: Index of the first triangle sourced from the first triangle node.
    * 28-31: Child index relative to the parent node. 0xF in case of the root
    *        node.
    */
   uint32_t node_info;
   int32_t relative_parent_index;
   uint32_t relative_child_index; /* Relative base index of child nodes. */
   struct nvk_internal_child children[NVK_MAX_CHILDREN_PER_NODE];
};

#define NVK_MAX_TRIANGLES_PER_NODE 5
#define NVK_MAX_VERTICES_PER_NODE  (NVK_MAX_TRIANGLES_PER_NODE * 3)

struct nvk_triangle_node {
   /* v0 of the first triangle. FLT_MAX for triangles with zero area. */
   struct nvk_vec3 v0;

   /* Tightly packed and compressed triangle data according to encoding_control.
    */
   uint32_t vertex_data[24];

   /* For dwords of unused space? */
   uint32_t unknown27;
   uint32_t unknown28;
   uint32_t unknown29;
   uint32_t primitive_index;

   /*
    * bits : usage
    * 0 -4 : X component bit size - 1
    * 5 -9 : Y component bit size - 1
    * 10-14: Z component bit size - 1
    * 15-19: Relative primitive index base - 1
    * 24-31: First bit stored. Bits that are outside the
    *        [first_bit,first_bit+bit_size-1] range are sourced from the
    *        corresponding v0 component.
    */
   uint32_t encoding_control;
};

enum nvk_instance_flags {
   nvk_instance_flag_flip_facing = (1 << 0),
   nvk_instance_flag_disable_face_culling = (1 << 1),
   nvk_instance_flag_force_opaque = (1 << 3),
   nvk_instance_flag_force_non_opaque = (1 << 4),
};

struct nvk_instance_node {
   uint32_t sbt_offset;
   uint32_t instance_index; /* +1 */
   uint32_t root_node_addr;
   uint32_t flags; /* 5 msb */
   float inv_transform[3][4];
};

#endif
