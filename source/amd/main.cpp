/*
 * Copyright © 2025 Valve Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "builder.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

struct amd_as_header {
   uint32_t reserved0;
   uint32_t reserved1;
   uint32_t size;
   uint32_t primitive_count;
   uint32_t active_primitive_count;
   uint32_t reserved5;
   uint32_t reserved6;
   uint32_t reserved7;
   uint32_t internal_nodes_offset;
};

struct radv_bvh_triangle_node {
   float coords[3][3];
   uint32_t reserved[3];
   /* flags in upper 4 bits */
   uint32_t geometry_id_and_flags;
   uint32_t triangle_id;
   uint32_t reserved2;
   uint32_t id;
};

struct radv_bvh_aabb_node {
   uint32_t primitive_id;
   /* flags in upper 4 bits */
   uint32_t geometry_id_and_flags;
   uint32_t reserved[14];
};

struct radv_bvh_box16_node {
   uint32_t children[4];
   uint16_t coords[4][2][3];
};

struct vk_aabb {
   vec3 min;
   vec3 max;
};

struct radv_bvh_box32_node {
   uint32_t children[4];
   vk_aabb coords[4];
   uint32_t reserved[4];
};

#define radv_bvh_node_triangle 0
#define radv_bvh_node_box16    4
#define radv_bvh_node_box32    5
#define radv_bvh_node_instance 6
#define radv_bvh_node_aabb     7

static void
print_node(uint8_t *data, uint32_t id, uint32_t indentation)
{
   uint32_t type = id & 7;
   uint32_t offset = (id & ~7u) << 3;

   char *indent = (char *)calloc(indentation * 3 + 1, 1);
   for (uint32_t i = 0; i < indentation * 3; i++)
      indent[i] = ' ';

   if (type == radv_bvh_node_box32) {
      printf("%sbox32\n", indent);
      radv_bvh_box32_node *node = (radv_bvh_box32_node *)(data + offset);
      for (uint32_t i = 0; i < 4; i++) {
         printf("%s   children[%u] = %x\n", indent, i, node->children[i]);
         vk_aabb bounds = node->coords[i];
         printf("%s   bounds[%u] = (%f %f %f) (%f %f %f)\n", indent, i,
                bounds.min.x, bounds.min.y, bounds.min.z, bounds.max.x, bounds.max.y, bounds.max.z);
         if (node->children[i] != 0xFFFFFFFF)
            print_node(data, node->children[i], indentation + 1);
      }
   } else if (type < radv_bvh_node_triangle + 4) {
      printf("%striangle%u\n", indent, type);
      radv_bvh_triangle_node *node = (radv_bvh_triangle_node *)(data + offset);
      for (uint32_t i = 0; i < 3; i++) {
         printf("%s   coords[%u] = (%f %f %f)\n", indent, i,
                node->coords[i][0], node->coords[i][1], node->coords[i][2]);
      }
      printf("%s   triangle_id = %u\n", indent, node->triangle_id);
      printf("%s   geometry_id = %u\n", indent, node->geometry_id_and_flags & 0xFFFFFF);
      printf("%s   flags = %u\n", indent, node->geometry_id_and_flags >> 24);
      printf("%s   id = %u\n", indent, node->id);
   } else {
      printf("%snode type not handled\n", indent);
   }

   free(indent);
}

static void
manual_re(bvh_builder *b)
{
   std::vector<vec3> vertices;
   for (uint32_t i = 0; i < 8; i++) {
      vertices.push_back(vec3(i + 0.1, i + 0.2, i + 0.3));
      vertices.push_back(vec3(i + 0.1, i + 0.5, i + 0.6));
      vertices.push_back(vec3(i + 0.1, i + 0.8, i + 0.9));
   }

   std::vector<VkAccelerationStructureGeometryKHR> geometries;
   std::vector<uint32_t> primitive_counts;

   VkDeviceOrHostAddressConstKHR vertex_data = {
      .hostAddress = vertices.data(),
   };

   VkAccelerationStructureGeometryTrianglesDataKHR triangles = {
      .sType =
         VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_TRIANGLES_DATA_KHR,
      .vertexFormat = VK_FORMAT_R32G32B32_SFLOAT,
      .vertexData = vertex_data,
      .vertexStride = sizeof(vec3),
      .maxVertex = (uint32_t)vertices.size() - 1,
      .indexType = VK_INDEX_TYPE_NONE_KHR,
   };

   VkAccelerationStructureGeometryDataKHR geometry_data = {
      .triangles = triangles,
   };

   VkAccelerationStructureGeometryKHR geometry = {
      .sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_KHR,
      .geometryType = VK_GEOMETRY_TYPE_TRIANGLES_KHR,
      .geometry = geometry_data,
      .flags = 1,
   };

   geometries.push_back(geometry);
   primitive_counts.push_back(vertices.size() / 3);

   struct bvh_blueprint blas_blueprint = {
      .geometries = geometries,
      .primitive_counts = primitive_counts,
      .type = VK_ACCELERATION_STRUCTURE_TYPE_BOTTOM_LEVEL_KHR,
   };

   struct bvh blas = bvh_builder_build(b, &blas_blueprint);

   assert(blas.buffer.size % 4 == 0);

   uint32_t *dwords = (uint32_t *)blas.buffer.data;
   float *floats = (float *)blas.buffer.data;

   printf("size: %08x\n", blas.buffer.size);
   for (uint32_t i = 0; i < blas.buffer.size / 4; i++) {
      printf("%08x: %08x %f\n", i * 4, dwords[i], floats[i]);
   }

   uint32_t header_offset = dwords[2];
   amd_as_header *header = (amd_as_header *)((uint8_t *)blas.buffer.data + header_offset);

   print_node((uint8_t *)header, (header->internal_nodes_offset >> 3) | radv_bvh_node_box32, 0);

   bvh_builder_bvh_finish(b, blas);
}

int
main()
{
   struct bvh_builder b;
   bvh_builder_init(&b);

   char state[32];
   initstate(2938475, state, 32);
   setstate(state);

   manual_re(&b);

   bvh_builder_finish(&b);

   return 0;
}
