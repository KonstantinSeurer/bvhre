/*
 * Copyright © 2023 Valve Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "builder.h"
#include "shaders/common.h"

#include <assert.h>
#include <stdio.h>
#include <string.h>

#include <vector>

static const uint32_t opaque_intersection_type_spv[] = {
#include "shaders/opaque_intersection_type.spv.h"
};

static void
assert_success(VkResult result)
{
   assert(result == VK_SUCCESS);
}

static const char *enabled_extensions[3] = {
   "VK_KHR_deferred_host_operations",
   "VK_KHR_acceleration_structure",
   "VK_KHR_ray_query",
};

void
bvh_builder_init(struct bvh_builder *b)
{
   memset(b, 0, sizeof(*b));

   VkApplicationInfo app_info = {
      .sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
      .pApplicationName = "bvhre",
      .applicationVersion = VK_MAKE_VERSION(1, 0, 0),
      .pEngineName = "bvhre",
      .engineVersion = VK_MAKE_VERSION(1, 0, 0),
      .apiVersion = VK_API_VERSION_1_3,
   };

   VkInstanceCreateInfo instance_ci = {
      .sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
      .pApplicationInfo = &app_info,
   };

   assert_success(vkCreateInstance(&instance_ci, NULL, &b->instance));

   uint32_t pdevice_count;
   assert_success(
      vkEnumeratePhysicalDevices(b->instance, &pdevice_count, NULL));
   assert(pdevice_count);

   std::vector<VkPhysicalDevice> pdevices(pdevice_count);
   assert_success(
      vkEnumeratePhysicalDevices(b->instance, &pdevice_count, pdevices.data()));

   for (VkPhysicalDevice pdevice : pdevices) {
      VkPhysicalDeviceProperties pdevice_props;
      vkGetPhysicalDeviceProperties(pdevice, &pdevice_props);

      if (pdevice_props.deviceType != VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU)
         continue;

      // if (pdevice_props.vendorID != 0x10de)
      //    continue;

      b->pdevice = pdevice;
      printf("Using device \"%s\"\n", pdevice_props.deviceName);
      break;
   }

   float priority = 1.0;

   VkDeviceQueueCreateInfo queue_ci = {
      .sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
      .queueFamilyIndex = 0xFFFFFFFF,
      .queueCount = 1,
      .pQueuePriorities = &priority,
   };

   uint32_t queue_family_count;
   vkGetPhysicalDeviceQueueFamilyProperties(b->pdevice, &queue_family_count,
                                            NULL);

   std::vector<VkQueueFamilyProperties> queue_family_props(queue_family_count);
   vkGetPhysicalDeviceQueueFamilyProperties(b->pdevice, &queue_family_count,
                                            queue_family_props.data());

   for (uint32_t i = 0; i < queue_family_count; i++) {
      uint32_t general_flags = VK_QUEUE_GRAPHICS_BIT | VK_QUEUE_COMPUTE_BIT;
      if ((queue_family_props[i].queueFlags & general_flags) == general_flags) {
         queue_ci.queueFamilyIndex = i;
         break;
      }
   }

   assert(queue_ci.queueFamilyIndex != 0xFFFFFFFF);
   b->queue_family_index = queue_ci.queueFamilyIndex;

   VkPhysicalDeviceRayQueryFeaturesKHR ray_query_features = {
      .sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_QUERY_FEATURES_KHR,
      .rayQuery = true,
   };

   VkPhysicalDeviceAccelerationStructureFeaturesKHR accel_struct_features = {
      .sType =
         VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ACCELERATION_STRUCTURE_FEATURES_KHR,
      .pNext = &ray_query_features,
      .accelerationStructure = true,
   };

   VkPhysicalDeviceVulkan12Features features12 = {
      .sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_2_FEATURES,
      .pNext = &accel_struct_features,
      .bufferDeviceAddress = true,
   };

   VkPhysicalDeviceFeatures features = {
      .shaderInt64 = true,
   };

   VkPhysicalDeviceFeatures2 features2 = {
      .sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2,
      .pNext = &features12,
      .features = features,
   };

   VkDeviceCreateInfo device_ci = {
      .sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
      .pNext = &features2,
      .queueCreateInfoCount = 1,
      .pQueueCreateInfos = &queue_ci,
      .enabledExtensionCount =
         sizeof(enabled_extensions) / sizeof(enabled_extensions[0]),
      .ppEnabledExtensionNames = enabled_extensions,
   };

   assert_success(vkCreateDevice(b->pdevice, &device_ci, NULL, &b->device));

   vkGetDeviceQueue(b->device, queue_ci.queueFamilyIndex, 0, &b->queue);

   VkCommandPoolCreateInfo pool_ci = {
      .sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
      .flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT,
      .queueFamilyIndex = queue_ci.queueFamilyIndex,
   };

   assert_success(vkCreateCommandPool(b->device, &pool_ci, NULL, &b->pool));

   VkCommandBufferAllocateInfo cmd_buffer_ai = {
      .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
      .commandPool = b->pool,
      .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
      .commandBufferCount = 1,
   };

   vkAllocateCommandBuffers(b->device, &cmd_buffer_ai, &b->cmd_buffer);

   VkPushConstantRange push_constant_range = {
      .stageFlags = VK_SHADER_STAGE_COMPUTE_BIT,
      .size = sizeof(struct shader_args),
   };

   VkPipelineLayoutCreateInfo pipeline_layout_ci = {
      .sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
      .pushConstantRangeCount = 1,
      .pPushConstantRanges = &push_constant_range,
   };

   assert_success(vkCreatePipelineLayout(b->device, &pipeline_layout_ci, NULL,
                                         &b->pipeline_layout));

   VkShaderModuleCreateInfo module_ci = {
      .sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
      .codeSize = sizeof(opaque_intersection_type_spv),
      .pCode = opaque_intersection_type_spv,
   };

   assert_success(
      vkCreateShaderModule(b->device, &module_ci, NULL, &b->shader_module));

   VkPipelineShaderStageCreateInfo stage_ci = {
      .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
      .stage = VK_SHADER_STAGE_COMPUTE_BIT,
      .module = b->shader_module,
      .pName = "main",
   };

   VkComputePipelineCreateInfo pipeline_ci = {
      .sType = VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO,
      .stage = stage_ci,
      .layout = b->pipeline_layout,
   };

   assert_success(vkCreateComputePipelines(b->device, VK_NULL_HANDLE, 1,
                                           &pipeline_ci, NULL, &b->pipeline));

   b->vkGetAccelerationStructureBuildSizesKHR =
      (PFN_vkGetAccelerationStructureBuildSizesKHR)vkGetDeviceProcAddr(
         b->device, "vkGetAccelerationStructureBuildSizesKHR");
   b->vkCreateAccelerationStructureKHR =
      (PFN_vkCreateAccelerationStructureKHR)vkGetDeviceProcAddr(
         b->device, "vkCreateAccelerationStructureKHR");
   b->vkCmdBuildAccelerationStructuresKHR =
      (PFN_vkCmdBuildAccelerationStructuresKHR)vkGetDeviceProcAddr(
         b->device, "vkCmdBuildAccelerationStructuresKHR");
   b->vkGetAccelerationStructureDeviceAddressKHR =
      (PFN_vkGetAccelerationStructureDeviceAddressKHR)vkGetDeviceProcAddr(
         b->device, "vkGetAccelerationStructureDeviceAddressKHR");
   b->vkDestroyAccelerationStructureKHR =
      (PFN_vkDestroyAccelerationStructureKHR)vkGetDeviceProcAddr(
         b->device, "vkDestroyAccelerationStructureKHR");
}

void
bvh_builder_finish(struct bvh_builder *b)
{
   vkDestroyPipeline(b->device, b->pipeline, NULL);
   vkDestroyShaderModule(b->device, b->shader_module, NULL);
   vkDestroyPipelineLayout(b->device, b->pipeline_layout, NULL);
   vkDestroyCommandPool(b->device, b->pool, NULL);
   vkDestroyDevice(b->device, NULL);
   vkDestroyInstance(b->instance, NULL);
}

static uint32_t
find_memory_index(struct bvh_builder *b, uint32_t supported_types,
                  VkMemoryPropertyFlags flags)
{
   VkPhysicalDeviceMemoryProperties properties = {};
   vkGetPhysicalDeviceMemoryProperties(b->pdevice, &properties);

   for (uint32_t i = 0; i < properties.memoryTypeCount; ++i) {
      if (supported_types & (1 << i)) {
         if ((properties.memoryTypes[i].propertyFlags & flags) == flags) {
            return i;
         }
      }
   }

   assert(false);
   return 0xFFFFFFFF;
}

static struct buffer
create_buffer(struct bvh_builder *b, uint32_t size, uint32_t usage)
{
   struct buffer result = {
      .size = size,
   };

   VkBufferCreateInfo buffer_info = {
      .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
      .size = size,
      .usage = usage,
      .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
      .queueFamilyIndexCount = 1,
      .pQueueFamilyIndices = &b->queue_family_index,
   };

   assert_success(
      vkCreateBuffer(b->device, &buffer_info, NULL, &result.buffer));

   VkMemoryRequirements memory_requirements = {};
   vkGetBufferMemoryRequirements(b->device, result.buffer,
                                 &memory_requirements);

   VkMemoryAllocateFlagsInfo flags_info = {
      .sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_FLAGS_INFO,
      .flags = (usage & VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT)
                  ? VK_MEMORY_ALLOCATE_DEVICE_ADDRESS_BIT
                  : 0u,
   };

   VkMemoryAllocateInfo alloc_info = {
      .sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
      .pNext = &flags_info,
      .allocationSize = memory_requirements.size,
      .memoryTypeIndex =
         find_memory_index(b, memory_requirements.memoryTypeBits,
                           VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT |
                              VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
                              VK_MEMORY_PROPERTY_HOST_COHERENT_BIT),
   };

   assert_success(
      vkAllocateMemory(b->device, &alloc_info, NULL, &result.memory));

   assert_success(
      vkBindBufferMemory(b->device, result.buffer, result.memory, 0));

   assert_success(
      vkMapMemory(b->device, result.memory, 0, size, 0, &result.data));

   memset(result.data, 0, size);

   return result;
}

static VkDeviceAddress
buffer_get_address(struct bvh_builder *b, const struct buffer &buffer)
{
   VkBufferDeviceAddressInfo info = {
      .sType = VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO,
      .buffer = buffer.buffer,
   };

   return vkGetBufferDeviceAddress(b->device, &info);
}

static void
buffer_finish(struct bvh_builder *b, struct buffer buffer)
{
   vkDestroyBuffer(b->device, buffer.buffer, NULL);
   if (buffer.memory)
      vkUnmapMemory(b->device, buffer.memory);
   vkFreeMemory(b->device, buffer.memory, NULL);
}

void
bvh_blueprint_init_ci(struct bvh_blueprint *blueprint,
                      VkAccelerationStructureBuildGeometryInfoKHR *info)
{
   *info = (VkAccelerationStructureBuildGeometryInfoKHR){
      .sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_GEOMETRY_INFO_KHR,
      .type = blueprint->type,
      .mode = VK_BUILD_ACCELERATION_STRUCTURE_MODE_BUILD_KHR,
      .geometryCount = (uint32_t)blueprint->geometries.size(),
      .pGeometries = blueprint->geometries.data(),
   };
}

struct bvh
bvh_builder_create_bvh(struct bvh_builder *b, uint32_t size)
{
   struct bvh result = {};

   result.buffer = create_buffer(
      b, size, VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_STORAGE_BIT_KHR);

   VkAccelerationStructureCreateInfoKHR accel_struct_info = {
      .sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_CREATE_INFO_KHR,
      .buffer = result.buffer.buffer,
      .size = size,
      .type = VK_ACCELERATION_STRUCTURE_TYPE_GENERIC_KHR,
   };

   assert_success(b->vkCreateAccelerationStructureKHR(
      b->device, &accel_struct_info, NULL, &result.accel_struct));

   return result;
}

struct bvh
bvh_builder_build(struct bvh_builder *b, struct bvh_blueprint *blueprint)
{
   assert(blueprint->geometries.size() == blueprint->primitive_counts.size());

   VkAccelerationStructureBuildGeometryInfoKHR info;
   bvh_blueprint_init_ci(blueprint, &info);

   std::vector<VkAccelerationStructureBuildRangeInfoKHR> ranges(
      blueprint->geometries.size());
   std::vector<buffer> data_buffers(blueprint->geometries.size());

   uint32_t primitive_count = 0;
   for (uint32_t i = 0; i < blueprint->geometries.size(); i++) {
      uint32_t count = blueprint->primitive_counts[i];
      primitive_count += count;

      VkAccelerationStructureBuildRangeInfoKHR range = {
         .primitiveCount = count,
      };
      ranges[i] = range;

      const VkAccelerationStructureGeometryKHR *geometry = &info.pGeometries[i];

      switch (geometry->geometryType) {
      case VK_GEOMETRY_TYPE_TRIANGLES_KHR: {
         uint32_t size = geometry->geometry.triangles.vertexStride * count * 3;

         buffer data_buffer = create_buffer(
            b, size,
            VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_BUILD_INPUT_READ_ONLY_BIT_KHR |
               VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT);
         data_buffers[i] = data_buffer;

         memcpy(data_buffer.data,
                geometry->geometry.triangles.vertexData.hostAddress, size);
         VkDeviceAddress *data_address = (VkDeviceAddress *)&geometry->geometry
                                            .triangles.vertexData.deviceAddress;
         *data_address = buffer_get_address(b, data_buffer);

         break;
      }
      case VK_GEOMETRY_TYPE_AABBS_KHR: {
         uint32_t size = geometry->geometry.aabbs.stride * count;

         buffer data_buffer = create_buffer(
            b, size,
            VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_BUILD_INPUT_READ_ONLY_BIT_KHR |
               VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT);
         data_buffers[i] = data_buffer;

         memcpy(data_buffer.data, geometry->geometry.aabbs.data.hostAddress,
                size);
         VkDeviceAddress *data_address =
            (VkDeviceAddress *)&geometry->geometry.aabbs.data.deviceAddress;
         *data_address = buffer_get_address(b, data_buffer);

         break;
      }
      case VK_GEOMETRY_TYPE_INSTANCES_KHR: {
         uint32_t size = count * sizeof(VkAccelerationStructureInstanceKHR);

         buffer data_buffer = create_buffer(
            b, size,
            VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_BUILD_INPUT_READ_ONLY_BIT_KHR |
               VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT);
         data_buffers[i] = data_buffer;

         memcpy(data_buffer.data, geometry->geometry.instances.data.hostAddress,
                size);
         VkDeviceAddress *data_address =
            (VkDeviceAddress *)&geometry->geometry.instances.data.deviceAddress;
         *data_address = buffer_get_address(b, data_buffer);
         break;
      }
      default:
         assert(false);
      }
   }

   VkAccelerationStructureBuildSizesInfoKHR size_info = {
      .sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_SIZES_INFO_KHR,
   };

   b->vkGetAccelerationStructureBuildSizesKHR(
      b->device, VK_ACCELERATION_STRUCTURE_BUILD_TYPE_DEVICE_KHR, &info,
      blueprint->primitive_counts.data(), &size_info);

   struct bvh result =
      bvh_builder_create_bvh(b, size_info.accelerationStructureSize);

   struct buffer scratch =
      create_buffer(b, size_info.buildScratchSize,
                    VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT |
                       VK_BUFFER_USAGE_STORAGE_BUFFER_BIT);

   assert_success(vkResetCommandBuffer(b->cmd_buffer, 0));

   VkCommandBufferBeginInfo begin_info = {
      .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
      .flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
   };

   assert_success(vkBeginCommandBuffer(b->cmd_buffer, &begin_info));

   info.dstAccelerationStructure = result.accel_struct;
   info.scratchData.deviceAddress = buffer_get_address(b, scratch);

   VkAccelerationStructureBuildRangeInfoKHR *p_ranges = ranges.data();
   b->vkCmdBuildAccelerationStructuresKHR(b->cmd_buffer, 1, &info, &p_ranges);

   assert_success(vkEndCommandBuffer(b->cmd_buffer));

   VkSubmitInfo submit_info = {
      .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
      .commandBufferCount = 1,
      .pCommandBuffers = &b->cmd_buffer,
   };

   assert_success(vkQueueSubmit(b->queue, 1, &submit_info, VK_NULL_HANDLE));

   assert_success(vkQueueWaitIdle(b->queue));

   buffer_finish(b, scratch);

   for (buffer data_buffer : data_buffers)
      buffer_finish(b, data_buffer);

   return result;
}

uint64_t
bvh_builder_bvh_addr(struct bvh_builder *b, struct bvh bvh)
{
   VkAccelerationStructureDeviceAddressInfoKHR addr_info = {
      .sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_DEVICE_ADDRESS_INFO_KHR,
      .accelerationStructure = bvh.accel_struct,
   };
   return b->vkGetAccelerationStructureDeviceAddressKHR(b->device, &addr_info);
}

void
bvh_builder_bvh_render(struct bvh_builder *b, struct bvh bvh, uint32_t width,
                       uint32_t height)
{
   struct buffer output =
      create_buffer(b, width * height * sizeof(uint32_t),
                    VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT);

   struct shader_args args = {
      .output_addr = buffer_get_address(b, output),
      .width = width,
      .height = height,
   };

   args.accel_struct_addr = bvh_builder_bvh_addr(b, bvh);

   assert_success(vkResetCommandBuffer(b->cmd_buffer, 0));

   VkCommandBufferBeginInfo begin_info = {
      .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
      .flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
   };

   assert_success(vkBeginCommandBuffer(b->cmd_buffer, &begin_info));

   vkCmdBindPipeline(b->cmd_buffer, VK_PIPELINE_BIND_POINT_COMPUTE,
                     b->pipeline);

   vkCmdPushConstants(b->cmd_buffer, b->pipeline_layout,
                      VK_SHADER_STAGE_COMPUTE_BIT, 0, sizeof(args), &args);

   vkCmdDispatch(b->cmd_buffer, (width + WORKGROUP_SIZE - 1) / WORKGROUP_SIZE,
                 (height + WORKGROUP_SIZE - 1) / WORKGROUP_SIZE, 1);

   assert_success(vkEndCommandBuffer(b->cmd_buffer));

   VkSubmitInfo submit_info = {
      .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
      .commandBufferCount = 1,
      .pCommandBuffers = &b->cmd_buffer,
   };

   assert_success(vkQueueSubmit(b->queue, 1, &submit_info, VK_NULL_HANDLE));

   assert_success(vkQueueWaitIdle(b->queue));

   float *result = (float *)output.data;
   float min = 1000000.0;
   float max = -1000000.0;
   for (uint32_t i = 0; i < width * height; i++) {
      min = (result[i] < min) ? result[i] : min;
      max = (result[i] > max) ? result[i] : max;
   }

   printf("Writing results (min = %f, max = %f) to '/tmp/result.ppm'...\n", min,
          max);

   FILE *file = fopen("/tmp/result.ppm", "w");

   fprintf(file, "P3\n%u %u\n255\n", width, height);
   for (uint32_t y = 0; y < height; y++) {
      for (uint32_t x = 0; x < width; x++) {
         uint32_t color =
            (uint32_t)((result[x + y * width] - min) / (max - min) * 255.0);
         fprintf(file, "%u %u %u\n", color, color, color);
      }
   }

   fclose(file);

   buffer_finish(b, output);
}

void
bvh_builder_bvh_finish(struct bvh_builder *b, struct bvh bvh)
{
   b->vkDestroyAccelerationStructureKHR(b->device, bvh.accel_struct, NULL);
   buffer_finish(b, bvh.buffer);
}
