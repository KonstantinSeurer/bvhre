/*
 * Copyright © 2023 Valve Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "nvk_acceleration_structure.h"

#include <assert.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* TODO: Remove, once this is upstreamed. */
/*
 * Copyright © 2014 Intel Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
#define MIN2(A, B)              ((A) < (B) ? (A) : (B))
#define MAX2(A, B)              ((A) > (B) ? (A) : (B))
#define DIV_ROUND_UP(A, B)      (((A) + (B)-1) / (B))
#define ALIGN_POT(x, pot_align) (((x) + (pot_align)-1) & ~((pot_align)-1))

union fi {
   float f;
   int32_t i;
   uint32_t ui;
};

/**************************************************************************
 *
 * Copyright 2008 VMware, Inc.
 * All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sub license, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice (including the
 * next paragraph) shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT.
 * IN NO EVENT SHALL VMWARE AND/OR ITS SUPPLIERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 **************************************************************************/

static inline unsigned
fui(float f)
{
   union fi fi;
   fi.f = f;
   return fi.ui;
}

static inline float
uif(uint32_t ui)
{
   union fi fi;
   fi.ui = ui;
   return fi.f;
}

static bool
util_invert_mat4x4(float *out, const float *m)
{
   float wtmp[4][8];
   float m0, m1, m2, m3, s;
   float *r0, *r1, *r2, *r3;

#define MAT(m, r, c) (m)[(c)*4 + (r)]
#define SWAP_ROWS(a, b)                                                        \
   {                                                                           \
      float *_tmp = a;                                                         \
      (a) = (b);                                                               \
      (b) = _tmp;                                                              \
   }

   r0 = wtmp[0], r1 = wtmp[1], r2 = wtmp[2], r3 = wtmp[3];

   r0[0] = MAT(m, 0, 0), r0[1] = MAT(m, 0, 1), r0[2] = MAT(m, 0, 2),
   r0[3] = MAT(m, 0, 3), r0[4] = 1.0, r0[5] = r0[6] = r0[7] = 0.0,

   r1[0] = MAT(m, 1, 0), r1[1] = MAT(m, 1, 1), r1[2] = MAT(m, 1, 2),
   r1[3] = MAT(m, 1, 3), r1[5] = 1.0, r1[4] = r1[6] = r1[7] = 0.0,

   r2[0] = MAT(m, 2, 0), r2[1] = MAT(m, 2, 1), r2[2] = MAT(m, 2, 2),
   r2[3] = MAT(m, 2, 3), r2[6] = 1.0, r2[4] = r2[5] = r2[7] = 0.0,

   r3[0] = MAT(m, 3, 0), r3[1] = MAT(m, 3, 1), r3[2] = MAT(m, 3, 2),
   r3[3] = MAT(m, 3, 3), r3[7] = 1.0, r3[4] = r3[5] = r3[6] = 0.0;

   /* choose pivot - or die */
   if (fabsf(r3[0]) > fabsf(r2[0]))
      SWAP_ROWS(r3, r2);
   if (fabsf(r2[0]) > fabsf(r1[0]))
      SWAP_ROWS(r2, r1);
   if (fabsf(r1[0]) > fabsf(r0[0]))
      SWAP_ROWS(r1, r0);
   if (0.0F == r0[0])
      return false;

   /* eliminate first variable     */
   m1 = r1[0] / r0[0];
   m2 = r2[0] / r0[0];
   m3 = r3[0] / r0[0];
   s = r0[1];
   r1[1] -= m1 * s;
   r2[1] -= m2 * s;
   r3[1] -= m3 * s;
   s = r0[2];
   r1[2] -= m1 * s;
   r2[2] -= m2 * s;
   r3[2] -= m3 * s;
   s = r0[3];
   r1[3] -= m1 * s;
   r2[3] -= m2 * s;
   r3[3] -= m3 * s;
   s = r0[4];
   if (s != 0.0F) {
      r1[4] -= m1 * s;
      r2[4] -= m2 * s;
      r3[4] -= m3 * s;
   }
   s = r0[5];
   if (s != 0.0F) {
      r1[5] -= m1 * s;
      r2[5] -= m2 * s;
      r3[5] -= m3 * s;
   }
   s = r0[6];
   if (s != 0.0F) {
      r1[6] -= m1 * s;
      r2[6] -= m2 * s;
      r3[6] -= m3 * s;
   }
   s = r0[7];
   if (s != 0.0F) {
      r1[7] -= m1 * s;
      r2[7] -= m2 * s;
      r3[7] -= m3 * s;
   }

   /* choose pivot - or die */
   if (fabsf(r3[1]) > fabsf(r2[1]))
      SWAP_ROWS(r3, r2);
   if (fabsf(r2[1]) > fabsf(r1[1]))
      SWAP_ROWS(r2, r1);
   if (0.0F == r1[1])
      return false;

   /* eliminate second variable */
   m2 = r2[1] / r1[1];
   m3 = r3[1] / r1[1];
   r2[2] -= m2 * r1[2];
   r3[2] -= m3 * r1[2];
   r2[3] -= m2 * r1[3];
   r3[3] -= m3 * r1[3];
   s = r1[4];
   if (0.0F != s) {
      r2[4] -= m2 * s;
      r3[4] -= m3 * s;
   }
   s = r1[5];
   if (0.0F != s) {
      r2[5] -= m2 * s;
      r3[5] -= m3 * s;
   }
   s = r1[6];
   if (0.0F != s) {
      r2[6] -= m2 * s;
      r3[6] -= m3 * s;
   }
   s = r1[7];
   if (0.0F != s) {
      r2[7] -= m2 * s;
      r3[7] -= m3 * s;
   }

   /* choose pivot - or die */
   if (fabsf(r3[2]) > fabsf(r2[2]))
      SWAP_ROWS(r3, r2);
   if (0.0F == r2[2])
      return false;

   /* eliminate third variable */
   m3 = r3[2] / r2[2];
   r3[3] -= m3 * r2[3], r3[4] -= m3 * r2[4], r3[5] -= m3 * r2[5],
      r3[6] -= m3 * r2[6], r3[7] -= m3 * r2[7];

   /* last check */
   if (0.0F == r3[3])
      return false;

   s = 1.0F / r3[3]; /* now back substitute row 3 */
   r3[4] *= s;
   r3[5] *= s;
   r3[6] *= s;
   r3[7] *= s;

   m2 = r2[3]; /* now back substitute row 2 */
   s = 1.0F / r2[2];
   r2[4] = s * (r2[4] - r3[4] * m2), r2[5] = s * (r2[5] - r3[5] * m2),
   r2[6] = s * (r2[6] - r3[6] * m2), r2[7] = s * (r2[7] - r3[7] * m2);
   m1 = r1[3];
   r1[4] -= r3[4] * m1, r1[5] -= r3[5] * m1, r1[6] -= r3[6] * m1,
      r1[7] -= r3[7] * m1;
   m0 = r0[3];
   r0[4] -= r3[4] * m0, r0[5] -= r3[5] * m0, r0[6] -= r3[6] * m0,
      r0[7] -= r3[7] * m0;

   m1 = r1[2]; /* now back substitute row 1 */
   s = 1.0F / r1[1];
   r1[4] = s * (r1[4] - r2[4] * m1), r1[5] = s * (r1[5] - r2[5] * m1),
   r1[6] = s * (r1[6] - r2[6] * m1), r1[7] = s * (r1[7] - r2[7] * m1);
   m0 = r0[2];
   r0[4] -= r2[4] * m0, r0[5] -= r2[5] * m0, r0[6] -= r2[6] * m0,
      r0[7] -= r2[7] * m0;

   m0 = r0[1]; /* now back substitute row 0 */
   s = 1.0F / r0[0];
   r0[4] = s * (r0[4] - r1[4] * m0), r0[5] = s * (r0[5] - r1[5] * m0),
   r0[6] = s * (r0[6] - r1[6] * m0), r0[7] = s * (r0[7] - r1[7] * m0);

   MAT(out, 0, 0) = r0[4];
   MAT(out, 0, 1) = r0[5], MAT(out, 0, 2) = r0[6];
   MAT(out, 0, 3) = r0[7], MAT(out, 1, 0) = r1[4];
   MAT(out, 1, 1) = r1[5], MAT(out, 1, 2) = r1[6];
   MAT(out, 1, 3) = r1[7], MAT(out, 2, 0) = r2[4];
   MAT(out, 2, 1) = r2[5], MAT(out, 2, 2) = r2[6];
   MAT(out, 2, 3) = r2[7], MAT(out, 3, 0) = r3[4];
   MAT(out, 3, 1) = r3[5], MAT(out, 3, 2) = r3[6];
   MAT(out, 3, 3) = r3[7];

#undef MAT
#undef SWAP_ROWS

   return true;
}
/* END */

struct nvk_aabb {
   union {
      struct {
         struct nvk_vec3 min;
         struct nvk_vec3 max;
      };
      float data[2][3];
   };
};

uint32_t
nvk_acceleration_structure_estimate_size(
   const VkAccelerationStructureBuildGeometryInfoKHR *info,
   uint32_t *primitive_counts)
{
   uint32_t primitive_count = 0;
   for (uint32_t i = 0; i < info->geometryCount; i++)
      primitive_count += primitive_counts[i];

   VkGeometryTypeKHR geometry_type = VK_GEOMETRY_TYPE_TRIANGLES_KHR;
   if (info->geometryCount) {
      geometry_type = info->pGeometries ? info->pGeometries[0].geometryType
                                        : info->ppGeometries[0][0].geometryType;
   }

   uint32_t leaf_node_size = 0;
   switch (geometry_type) {
   case VK_GEOMETRY_TYPE_TRIANGLES_KHR:
      leaf_node_size = sizeof(struct nvk_triangle_node);
      break;
   case VK_GEOMETRY_TYPE_INSTANCES_KHR:
      leaf_node_size = sizeof(struct nvk_instance_node);
      break;
   default:
      assert(false);
   }

   uint32_t internal_count = MAX2(primitive_count, 2) - 1;

   return sizeof(struct nvk_bvh_header) + primitive_count * leaf_node_size +
          internal_count * sizeof(struct nvk_internal_node);
}

static struct nvk_aabb
nvk_encode_instance_node(struct nvk_instance_node *dst,
                         const VkAccelerationStructureInstanceKHR *src,
                         uint32_t primitive_index)
{
   dst->sbt_offset = src->instanceShaderBindingTableRecordOffset;
   dst->instance_index = primitive_index + 1;

   assert((src->accelerationStructureReference >> 32) == 0);
   dst->root_node_addr = src->accelerationStructureReference + 0x300;

   dst->flags = 0;
   if (src->flags & VK_GEOMETRY_INSTANCE_TRIANGLE_FACING_CULL_DISABLE_BIT_KHR)
      dst->flags |= nvk_instance_flag_disable_face_culling << 27;
   if (src->flags & VK_GEOMETRY_INSTANCE_TRIANGLE_FLIP_FACING_BIT_KHR)
      dst->flags |= nvk_instance_flag_flip_facing << 27;
   if (src->flags & VK_GEOMETRY_INSTANCE_FORCE_OPAQUE_BIT_KHR)
      dst->flags |= nvk_instance_flag_force_opaque << 27;
   if (src->flags & VK_GEOMETRY_INSTANCE_FORCE_NO_OPAQUE_BIT_KHR)
      dst->flags |= nvk_instance_flag_force_non_opaque << 27;

   float transform[16], inv_transform[16];
   memcpy(transform, &src->transform.matrix, sizeof(src->transform.matrix));
   transform[12] = 0.0;
   transform[13] = 0.0;
   transform[14] = 0.0;
   transform[15] = 1.0;

   util_invert_mat4x4(inv_transform, transform);

   memcpy(dst->inv_transform, inv_transform, sizeof(dst->inv_transform));

   float blas_bounds[2][3] = {
      {0, 0, 0},
      {1, 1, 1},
   };

   struct nvk_aabb bounds;

   for (unsigned j = 0; j < 3; ++j) {
      bounds.data[0][j] = src->transform.matrix[j][3];
      bounds.data[1][j] = src->transform.matrix[j][3];
      for (unsigned k = 0; k < 3; ++k) {
         bounds.data[0][j] +=
            MIN2(src->transform.matrix[j][k] * blas_bounds[0][k],
                 src->transform.matrix[j][k] * blas_bounds[1][k]);
         bounds.data[1][j] +=
            MAX2(src->transform.matrix[j][k] * blas_bounds[0][k],
                 src->transform.matrix[j][k] * blas_bounds[1][k]);
      }
   }

   return bounds;
}

static struct nvk_aabb
nvk_encode_triangle_node(
   struct nvk_triangle_node *dst,
   const VkAccelerationStructureGeometryTrianglesDataKHR *data,
   uint32_t primitive_index)
{
   struct nvk_vec3 vertices[3];
   uint32_t indices[3] = {
      primitive_index * 3,
      primitive_index * 3 + 1,
      primitive_index * 3 + 2,
   };

   switch (data->indexType) {
   case VK_INDEX_TYPE_UINT8_EXT: {
      const uint8_t *src = data->indexData.hostAddress;
      indices[0] = src[indices[0]];
      indices[1] = src[indices[1]];
      indices[2] = src[indices[2]];
      break;
   }
   case VK_INDEX_TYPE_UINT16: {
      const uint16_t *src = data->indexData.hostAddress;
      indices[0] = src[indices[0]];
      indices[1] = src[indices[1]];
      indices[2] = src[indices[2]];
      break;
   }
   case VK_INDEX_TYPE_UINT32: {
      const uint32_t *src = data->indexData.hostAddress;
      indices[0] = src[indices[0]];
      indices[1] = src[indices[1]];
      indices[2] = src[indices[2]];
      break;
   }
   default:
      break;
   }

   struct nvk_aabb bounds = {
      .min = {INFINITY, INFINITY, INFINITY},
      .max = {-INFINITY, -INFINITY, -INFINITY},
   };

   for (uint32_t i = 0; i < 3; i++) {
      const void *_src = (const uint8_t *)data->vertexData.hostAddress +
                         (indices[i] * data->vertexStride);

      switch (data->vertexFormat) {
      case VK_FORMAT_R32G32B32_SFLOAT: {
         const float *src = _src;
         vertices[i].x = src[0];
         vertices[i].y = src[1];
         vertices[i].z = src[2];
         break;
      }
      default:
         assert(false);
      }

      bounds.min.x = MIN2(bounds.min.x, vertices[i].x);
      bounds.min.y = MIN2(bounds.min.y, vertices[i].y);
      bounds.min.z = MIN2(bounds.min.z, vertices[i].z);
      bounds.max.x = MAX2(bounds.max.x, vertices[i].x);
      bounds.max.y = MAX2(bounds.max.y, vertices[i].y);
      bounds.max.z = MAX2(bounds.max.z, vertices[i].z);
   }

   memcpy(&dst->v0, vertices, sizeof(vertices));
   dst->encoding_control = 0x20000000 | (31 << 10) | (31 << 5) | 31;

   return bounds;
}

struct nvk_internal_ir_node {
   struct nvk_aabb bounds[2];
   uint32_t indices[2];
};

#define NVK_INVALID_CHILD_INDEX 0xFFFFFFFF

enum nvk_node_type
nvk_node_type_from_geometry_type(VkGeometryTypeKHR geometry_type)
{
   switch (geometry_type) {
   case VK_GEOMETRY_TYPE_TRIANGLES_KHR:
      return nvk_node_triangle;
   case VK_GEOMETRY_TYPE_INSTANCES_KHR:
      return nvk_node_instance;
   default:
      assert(false);
      return 0;
   };
}

static uint32_t
nvk_build_ir_naive(struct nvk_internal_ir_node *ir,
                   struct nvk_aabb *leaf_bounds, uint32_t primitive_count,
                   VkGeometryTypeKHR geometry_type)
{
   enum nvk_node_type leaf_node_type =
      nvk_node_type_from_geometry_type(geometry_type);

   for (uint32_t i = 0; i < primitive_count; i++) {
      struct nvk_internal_ir_node *ir_node = &ir[i / 2];
      ir_node->indices[i % 2] = (i << 2) | leaf_node_type;
      ir_node->bounds[i % 2] = leaf_bounds[i];
   }

   if (primitive_count % 2 != 0) {
      struct nvk_internal_ir_node *ir_node = &ir[primitive_count / 2];
      ir_node->bounds[1] = (struct nvk_aabb){
         .min = {INFINITY, INFINITY, INFINITY},
         .max = {-INFINITY, -INFINITY, -INFINITY},
      };
      ir_node->indices[1] = NVK_INVALID_CHILD_INDEX;
   }

   if (primitive_count <= 2)
      return 0;

   uint32_t src_count = DIV_ROUND_UP(primitive_count, 2);
   uint32_t first_src = 0;

   while (true) {
      for (uint32_t i = 0; i < src_count; i++) {
         struct nvk_internal_ir_node *src_node = &ir[first_src + i];
         struct nvk_internal_ir_node *dst_node =
            &ir[first_src + src_count + i / 2];

         dst_node->bounds[i % 2] = (struct nvk_aabb){
            .min.x = MIN2(src_node->bounds[0].min.x, src_node->bounds[1].min.x),
            .min.y = MIN2(src_node->bounds[0].min.y, src_node->bounds[1].min.y),
            .min.z = MIN2(src_node->bounds[0].min.z, src_node->bounds[1].min.z),
            .max.x = MAX2(src_node->bounds[0].max.x, src_node->bounds[1].max.x),
            .max.y = MAX2(src_node->bounds[0].max.y, src_node->bounds[1].max.y),
            .max.z = MAX2(src_node->bounds[0].max.z, src_node->bounds[1].max.z),
         };

         dst_node->indices[i % 2] = ((first_src + i) << 2) | nvk_node_internal;
      }

      if (src_count % 2 != 0) {
         struct nvk_internal_ir_node *ir_node =
            &ir[first_src + src_count + src_count / 2];
         ir_node->bounds[1] = (struct nvk_aabb){
            .min = {INFINITY, INFINITY, INFINITY},
            .max = {-INFINITY, -INFINITY, -INFINITY},
         };
         ir_node->indices[1] = NVK_INVALID_CHILD_INDEX;
      }

      first_src += src_count;
      src_count = DIV_ROUND_UP(src_count, 2);

      if (src_count == 1)
         return first_src;
   }

   return 0;
}

struct nvk_encode_context {
   void *dst;
   struct nvk_internal_ir_node *ir;
   uint32_t leaf_nodes_offset;
   uint32_t leaf_nodes_stride;
   bool tlas;
};

static uint32_t
nvk_encode_internal_node(struct nvk_encode_context *ctx, uint32_t ir_index,
                         uint32_t dst_offset, uint32_t parent_offset,
                         uint32_t child_nodes_offset, uint32_t child_index)
{
   struct nvk_internal_node *node = (void *)((uint8_t *)ctx->dst + dst_offset);
   node->unknown0 = ctx->tlas ? 0xa0 : 0x60;

   struct nvk_internal_ir_node *ir_node = ctx->ir + ir_index;

   node->min.x = MIN2(ir_node->bounds[0].min.x, ir_node->bounds[1].min.x);
   node->min.y = MIN2(ir_node->bounds[0].min.y, ir_node->bounds[1].min.y);
   node->min.z = MIN2(ir_node->bounds[0].min.z, ir_node->bounds[1].min.z);

   struct nvk_vec3 max = {
      .x = MAX2(ir_node->bounds[0].max.x, ir_node->bounds[1].max.x),
      .y = MAX2(ir_node->bounds[0].max.y, ir_node->bounds[1].max.y),
      .z = MAX2(ir_node->bounds[0].max.z, ir_node->bounds[1].max.z),
   };

   struct nvk_vec3 grid_size_log2 = {
      .x = MAX2(ceilf(log2f(max.x - node->min.x)), -0x7F),
      .y = MAX2(ceilf(log2f(max.y - node->min.y)), -0x7F),
      .z = MAX2(ceilf(log2f(max.z - node->min.z)), -0x7F),
   };

   struct nvk_vec3 grid_size = {
      .x = powf(2.0, grid_size_log2.x),
      .y = powf(2.0, grid_size_log2.y),
      .z = powf(2.0, grid_size_log2.z),
   };

   node->grid_size_exp[0] = (int8_t)grid_size_log2.x + 0x7F;
   node->grid_size_exp[1] = (int8_t)grid_size_log2.y + 0x7F;
   node->grid_size_exp[2] = (int8_t)grid_size_log2.z + 0x7F;

   uint32_t first_leaf_index = (ir_node->indices[0] & 0x3) == nvk_node_internal
                                  ? (ir_node->indices[1] >> 2)
                                  : (ir_node->indices[0] >> 2);

   node->relative_leaf_offset = ctx->leaf_nodes_offset +
                                first_leaf_index * ctx->leaf_nodes_stride -
                                dst_offset;

   node->node_info = (child_index << 28) | (ctx->tlas ? 0x1ffff : 0);

   node->relative_parent_index = parent_offset - dst_offset;
   node->relative_parent_index /= sizeof(struct nvk_internal_node);

   node->relative_child_index = child_nodes_offset - dst_offset;
   node->relative_child_index /= sizeof(struct nvk_internal_node);

   uint32_t internal_child_count = 0;
   for (uint32_t i = 0; i < 2; i++)
      if ((ir_node->indices[i] & 0x3) == nvk_node_internal &&
          ir_node->indices[i] != NVK_INVALID_CHILD_INDEX)
         internal_child_count++;

   uint32_t size = internal_child_count * sizeof(struct nvk_internal_node);
   uint32_t internal_child_child_offset = child_nodes_offset + size;

   for (uint32_t i = 0; i < NVK_MAX_CHILDREN_PER_NODE; i++) {
      if (i > 1 || ir_node->indices[i] == NVK_INVALID_CHILD_INDEX) {
         memset(&node->children[i], 0, sizeof(struct nvk_internal_child));
         node->children[i].min_z = 0xFF;
         continue;
      }

      node->children[i].cull_mask = 0xFF;
      node->children[i].packed_data = ((ir_node->indices[i] & 0x3) << 6);
      if ((ir_node->indices[i] & 0x3) != nvk_node_internal)
         node->children[i].packed_data |= 1;

      node->children[i].min_x = (uint8_t)MIN2(
         (ir_node->bounds[i].min.x - node->min.x) / grid_size.x * 256.0, 0xFF);
      node->children[i].max_x = (uint8_t)MIN2(
         (ir_node->bounds[i].max.x - node->min.x) / grid_size.x * 256.0, 0xFF);

      node->children[i].min_y = (uint8_t)MIN2(
         (ir_node->bounds[i].min.y - node->min.y) / grid_size.y * 256.0, 0xFF);
      node->children[i].max_y = (uint8_t)MIN2(
         (ir_node->bounds[i].max.y - node->min.y) / grid_size.y * 256.0, 0xFF);

      node->children[i].min_z = (uint8_t)MIN2(
         (ir_node->bounds[i].min.z - node->min.z) / grid_size.z * 256.0, 0xFF);
      node->children[i].max_z = (uint8_t)MIN2(
         (ir_node->bounds[i].max.z - node->min.z) / grid_size.z * 256.0, 0xFF);

      if ((ir_node->indices[i] & 0x3) == nvk_node_internal) {
         uint32_t child_child_size =
            nvk_encode_internal_node(
               ctx, ir_node->indices[i] >> 2,
               child_nodes_offset + i * sizeof(struct nvk_internal_node),
               dst_offset, internal_child_child_offset, i) -
            sizeof(struct nvk_internal_node);
         size += child_child_size;
         internal_child_child_offset += child_child_size;
      }
   }

   return size + sizeof(struct nvk_internal_node);
}

void
nvk_build_acceleration_structure(
   void *dst, const VkAccelerationStructureBuildGeometryInfoKHR *info,
   const VkAccelerationStructureBuildRangeInfoKHR *build_range_infos)
{
   memset(dst, 0, 0x600);

   struct nvk_bvh_header *header = dst;
   header->internal_metadata_offset = 0xFFFFFFFF;

   uint32_t dst_offset = sizeof(struct nvk_bvh_header);
   uint32_t leaf_nodes_offset =
      dst_offset + ((info->type == VK_ACCELERATION_STRUCTURE_TYPE_TOP_LEVEL_KHR)
                       ? 0
                       : 0x200);
   uint32_t leaf_nodes_stride = 0;

   VkGeometryTypeKHR geometry_type = VK_GEOMETRY_TYPE_TRIANGLES_KHR;

   uint32_t primitive_count = 0;
   for (uint32_t i = 0; i < info->geometryCount; i++)
      primitive_count += build_range_infos[i].primitiveCount;

   struct nvk_aabb *leaf_bounds =
      malloc(primitive_count * sizeof(struct nvk_aabb));

   uint32_t primitive_index = 0;
   for (uint32_t i = 0; i < info->geometryCount; i++) {
      const VkAccelerationStructureGeometryKHR *geometry;
      if (info->pGeometries)
         geometry = info->pGeometries + i;
      else
         geometry = info->ppGeometries[i];

      geometry_type = geometry->geometryType;

      const VkAccelerationStructureBuildRangeInfoKHR *range =
         build_range_infos + i;

      if (geometry->geometryType == VK_GEOMETRY_TYPE_INSTANCES_KHR) {
         leaf_nodes_stride = sizeof(struct nvk_instance_node);

         for (uint32_t p = 0; p < range->primitiveCount; p++) {
            const VkAccelerationStructureInstanceKHR *instance;
            if (geometry->geometry.instances.arrayOfPointers)
               instance = ((const VkAccelerationStructureInstanceKHR **)
                              geometry->geometry.instances.data.hostAddress)[p];
            else
               instance = (const VkAccelerationStructureInstanceKHR *)
                             geometry->geometry.instances.data.hostAddress +
                          p;

            leaf_bounds[primitive_index] =
               nvk_encode_instance_node((void *)((uint8_t *)dst + dst_offset),
                                        instance, primitive_index);

            dst_offset += sizeof(struct nvk_instance_node);
            primitive_index++;
         }
      } else if (geometry->geometryType == VK_GEOMETRY_TYPE_TRIANGLES_KHR) {
         leaf_nodes_stride = sizeof(struct nvk_triangle_node);

         for (uint32_t p = 0; p < range->primitiveCount; p++) {
            leaf_bounds[primitive_index] = nvk_encode_triangle_node(
               (void *)((uint8_t *)dst + dst_offset + 0x200),
               &geometry->geometry.triangles, primitive_index);

            dst_offset += sizeof(struct nvk_triangle_node);
            primitive_index++;
         }
      }
   }

   struct nvk_internal_ir_node *ir =
      malloc(primitive_count * sizeof(struct nvk_internal_ir_node));
   uint32_t root_index =
      nvk_build_ir_naive(ir, leaf_bounds, primitive_count, geometry_type);

   dst_offset = ALIGN_POT(dst_offset, 0x100);

   if (geometry_type == VK_GEOMETRY_TYPE_TRIANGLES_KHR)
      dst_offset = 0x300;

   header->internal_nodes_offset = dst_offset;

   struct nvk_encode_context encode_ctx = {
      .dst = dst,
      .ir = ir,
      .leaf_nodes_offset = leaf_nodes_offset,
      .leaf_nodes_stride = leaf_nodes_stride,
      .tlas = info->type == VK_ACCELERATION_STRUCTURE_TYPE_TOP_LEVEL_KHR,
   };

   nvk_encode_internal_node(&encode_ctx, root_index, dst_offset, dst_offset,
                            dst_offset + sizeof(struct nvk_internal_node), 0xF);
}
