/*
 * Copyright © 2023 Valve Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef BVHRE_BUILDER_H
#define BVHRE_BUILDER_H

#include "vulkan/vulkan.h"

#include <vector>

struct bvh_builder {
   VkInstance instance;
   VkDebugReportCallbackEXT debug_callback;
   VkPhysicalDevice pdevice;
   VkDevice device;
   VkQueue queue;
   VkCommandPool pool;
   VkCommandBuffer cmd_buffer;
   uint32_t queue_family_index;

   /* Pipeline used for traversal testing. */
   VkPipelineLayout pipeline_layout;
   VkShaderModule shader_module;
   VkPipeline pipeline;

   PFN_vkGetAccelerationStructureBuildSizesKHR
      vkGetAccelerationStructureBuildSizesKHR;
   PFN_vkCreateAccelerationStructureKHR vkCreateAccelerationStructureKHR;
   PFN_vkCmdBuildAccelerationStructuresKHR vkCmdBuildAccelerationStructuresKHR;
   PFN_vkGetAccelerationStructureDeviceAddressKHR
      vkGetAccelerationStructureDeviceAddressKHR;
   PFN_vkDestroyAccelerationStructureKHR vkDestroyAccelerationStructureKHR;
};

void bvh_builder_init(struct bvh_builder *b);

void bvh_builder_finish(struct bvh_builder *b);

struct buffer {
   VkBuffer buffer;
   VkDeviceMemory memory;
   void *data;
   uint32_t size;
};

struct bvh {
   struct buffer buffer;
   VkAccelerationStructureKHR accel_struct;
};

struct bvh_blueprint {
   std::vector<VkAccelerationStructureGeometryKHR> geometries;
   std::vector<uint32_t> primitive_counts;
   VkAccelerationStructureTypeKHR type;
};

void bvh_blueprint_init_ci(struct bvh_blueprint *blueprint,
                           VkAccelerationStructureBuildGeometryInfoKHR *info);

struct bvh bvh_builder_create_bvh(struct bvh_builder *b, uint32_t size);

struct bvh bvh_builder_build(struct bvh_builder *b,
                             struct bvh_blueprint *blueprint);

uint64_t bvh_builder_bvh_addr(struct bvh_builder *b, struct bvh bvh);

void bvh_builder_bvh_render(struct bvh_builder *b, struct bvh bvh,
                            uint32_t width, uint32_t height);

void bvh_builder_bvh_finish(struct bvh_builder *b, struct bvh bvh);

static inline float
bits_to_float(uint32_t u)
{
   return *(float *)&u;
}

static inline uint32_t
float_to_bits(float f)
{
   return *(uint32_t *)&f;
}

static inline uint32_t
bitview_read(uint32_t *dwords, uint32_t bit, uint32_t bit_count)
{
   uint32_t first_bit_count = bit % 32;
   uint32_t lower = dwords[bit / 32] >> first_bit_count;
   uint32_t upper =
      first_bit_count ? dwords[bit / 32 + 1] << (32 - first_bit_count) : 0;
   uint32_t total = lower | upper;

   return bit_count != 32 ? total & ((1u << bit_count) - 1u) : total;
}

struct vec3 {
   float x;
   float y;
   float z;

#ifdef __cplusplus
   vec3(float x, float y, float z): x(x), y(y), z(z)
   {
   }

   vec3(): x(0.0), y(0.0), z(0.0)
   {
   }
#endif
};

#endif
