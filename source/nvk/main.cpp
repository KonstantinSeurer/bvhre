/*
 * Copyright © 2023 Valve Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "../builder.h"

#include "nvk_acceleration_structure.h"
#include "nvk_bvh.h"

#include <assert.h>
#include <stdio.h>
#include <string.h>

#include <random>

static struct nvk_bvh_header expected_header_base = {
   .unknown0 = 0x100003,
   .unknown1 = 0x1,
   .leaf_nodes_offset = 0x380,
   .internal_nodes_offset = 0x300,
   .internal_metadata_offset = 0x1c80,
   .unknown16 = 0xffffffff,
   .unknown17 = 0xffffffff,
   .float_list_offset = 0x180,
};

static const char *
nvk_node_type_to_string(enum nvk_node_type type)
{
   switch (type) {
   case nvk_node_internal:
      return "internal";
   case nvk_node_instance:
      return "instance";
   case nvk_node_triangle:
      return "triangle";
   default:
      assert(false);
      return NULL;
   }
}

static struct nvk_vec3
nvk_internal_load_grid_size(struct nvk_internal_node *node)
{
   return nvk_vec3(bits_to_float(node->grid_size_exp[0] << 23),
                   bits_to_float(node->grid_size_exp[1] << 23),
                   bits_to_float(node->grid_size_exp[2] << 23));
}

static nvk_vec3
nvk_internal_child_min(struct nvk_internal_node *node,
                       struct nvk_internal_child *child)
{
   struct nvk_vec3 grid_size = nvk_internal_load_grid_size(node);

   nvk_vec3 result;
   result.x = node->min.x + grid_size.x * (float)child->min_x / 256.0;
   result.y = node->min.y + grid_size.y * (float)child->min_y / 256.0;
   result.z = node->min.z + grid_size.z * (float)child->min_z / 256.0;
   return result;
}

static nvk_vec3
nvk_internal_child_max(struct nvk_internal_node *node,
                       struct nvk_internal_child *child)
{
   struct nvk_vec3 grid_size = nvk_internal_load_grid_size(node);

   nvk_vec3 result;
   result.x = node->min.x + grid_size.x * ((float)child->max_x + 1.0) / 256.0;
   result.y = node->min.y + grid_size.y * ((float)child->max_y + 1.0) / 256.0;
   result.z = node->min.z + grid_size.z * ((float)child->max_z + 1.0) / 256.0;
   return result;
}

struct nvk_triangle_encoding_control {
   uint32_t first_bit;
   uint32_t bit_sizes[3];
};

static struct nvk_triangle_encoding_control
nvk_triangle_load_encoding_control(struct nvk_triangle_node *node)
{
   struct nvk_triangle_encoding_control result = {
      .first_bit = (node->encoding_control >> 24) & 0x1F,
   };

   result.bit_sizes[2] = ((node->encoding_control >> 10) & 0x1F) + 1;
   result.bit_sizes[1] = ((node->encoding_control >> 5) & 0x1F) + 1;
   result.bit_sizes[0] = ((node->encoding_control >> 0) & 0x1F) + 1;

   return result;
}

static float
random_float()
{
   return (random() - (1 << 30)) * 0.00001;
}

static void
print_bin(uint32_t v)
{
   for (uint32_t i = 0; i < 32; i++)
      printf("%u", (v & (1u << (31u - i))) ? 1u : 0u);
}

static void
print_bin_rev(uint32_t v)
{
   for (uint32_t i = 0; i < 32; i++)
      printf("%u", (v & (1u << i)) ? 1u : 0u);
}

static void
nvk_triangle_load_vertices(struct nvk_triangle_node *node, struct nvk_vec3 *out)
{
   out[0] = node->v0;
   float *v0 = (float *)&node->v0;

   struct nvk_triangle_encoding_control encoding_control =
      nvk_triangle_load_encoding_control(node);
   uint32_t bit = 0;

   uint32_t inherited_bits[3];
   for (uint32_t i = 0; i < 3; i++) {
      uint32_t bit_count =
         encoding_control.first_bit + encoding_control.bit_sizes[i];
      if (bit_count != 32)
         inherited_bits[i] = float_to_bits(v0[i]) & (0xFFFFFFFF << bit_count);
      else
         inherited_bits[i] = 0;

      inherited_bits[i] |=
         float_to_bits(v0[i]) & ((1u << encoding_control.first_bit) - 1u);
   }

   for (uint32_t i = 1; i < NVK_MAX_VERTICES_PER_NODE - 3; i++) {
      uint32_t comps[3];
      for (uint32_t c = 0; c < 3; c++) {
         uint32_t comp =
            bitview_read(node->vertex_data, bit, encoding_control.bit_sizes[c])
            << encoding_control.first_bit;
         comp |= inherited_bits[c];
         comps[c] = comp;

         bit += encoding_control.bit_sizes[c];
      }

      out[i].x = bits_to_float(comps[0]);
      out[i].y = bits_to_float(comps[1]);
      out[i].z = bits_to_float(comps[2]);
   }
}

static void
print_triangle(struct nvk_triangle_node *node, uint32_t indentation)
{
   char *indentation_string = (char *)calloc(indentation + 1, 1);
   for (uint32_t i = 0; i < indentation; i++)
      indentation_string[i] = ' ';

   struct nvk_vec3 v[NVK_MAX_VERTICES_PER_NODE];
   nvk_triangle_load_vertices(node, v);

   printf("%striangle:\n", indentation_string);
   for (uint32_t i = 0; i < NVK_MAX_VERTICES_PER_NODE; i++)
      printf("%s   v%u: %f %f %f\n", indentation_string, i, v[i].x, v[i].y,
             v[i].z);

   uint32_t primitive_index = node->primitive_index & 0x1FFFFFFF;
   printf("%s   primitive_index[0]: %u\n", indentation_string, primitive_index);

   // TODO: This breaks for higher differences
   uint32_t primitive_index_base_log2 = (node->encoding_control >> 15) & 0x1F;
   uint32_t relative_primitive_index =
      (node->unknown29 >> (15 - primitive_index_base_log2)) &
      ((1 << primitive_index_base_log2) - 1);
   printf("%s   primitive_index[1]: %u\n", indentation_string,
          primitive_index + (1 << primitive_index_base_log2) +
             relative_primitive_index);

   printf("%s   unknown27: ", indentation_string);
   print_bin(node->unknown27);
   printf("\n%s   unknown28: ", indentation_string);
   print_bin(node->unknown28);
   printf("\n%s   unknown29: ", indentation_string);
   print_bin(node->unknown29);
   printf("\n%s   encoding_control: ", indentation_string);
   print_bin(node->encoding_control);
   printf("\n");
}

static void
print_instance(struct nvk_instance_node *node, uint32_t indentation)
{
   char *indentation_string = (char *)calloc(indentation + 1, 1);
   for (uint32_t i = 0; i < indentation; i++)
      indentation_string[i] = ' ';

   printf("%sinstance:\n", indentation_string);
   printf("%s   sbt_offset: %u\n", indentation_string,
          node->sbt_offset & 0xFFFFFF);
   printf("%s   root_node_addr: %08x\n", indentation_string,
          node->root_node_addr);
   printf("%s   flags: %02x\n", indentation_string, node->flags >> 27);
   printf("%s   inv_transform:\n", indentation_string);
   for (uint32_t i = 0; i < 3; i++) {
      printf("%s      (%f %f %f %f)\n", indentation_string,
             node->inv_transform[i][0], node->inv_transform[i][1],
             node->inv_transform[i][2], node->inv_transform[i][3]);
   }

   free(indentation_string);
}

static void
print_internal(struct nvk_internal_node *node, uint32_t indentation)
{
   char *indentation_string = (char *)calloc(indentation + 1, 1);
   for (uint32_t i = 0; i < indentation; i++)
      indentation_string[i] = ' ';

   struct nvk_vec3 grid_size = nvk_internal_load_grid_size(node);
   assert(grid_size.x != 0);
   printf("%sinternal:\n", indentation_string);
   printf("%s   min: %f %f %f\n", indentation_string, node->min.x, node->min.y,
          node->min.z);
   printf("%s   grid_size: %f %f %f\n", indentation_string, grid_size.x,
          grid_size.y, grid_size.z);
   printf("%s   relative_parent_index: %i\n", indentation_string,
          node->relative_parent_index);
   printf("%s   relative_child_index: %u\n", indentation_string,
          node->relative_child_index);

   uint32_t triangle_base_index = (node->node_info >> 24) & 0xF;
   printf("%s   triangle_base_index: %u\n", indentation_string,
          triangle_base_index);
   printf("%s   parent_child_index: %u\n", indentation_string,
          node->node_info >> 28);
   // assert((node->node_info & 0xFFFFFF) == 0);

   uint32_t internal_index = 0;
   uint32_t instance_index = 0;
   uint32_t triangle_index = 0;
   for (uint32_t i = 0; i < NVK_MAX_CHILDREN_PER_NODE; i++) {
      struct nvk_internal_child *child = &node->children[i];
      if (child->cull_mask == 0)
         continue;

      struct nvk_vec3 min = nvk_internal_child_min(node, child);
      struct nvk_vec3 max = nvk_internal_child_max(node, child);

      enum nvk_node_type child_type =
         (enum nvk_node_type)(child->packed_data >> 6);

      printf("%s   %s %u:\n", indentation_string,
             nvk_node_type_to_string(child_type), i);
      printf("%s      min: %f %f %f\n", indentation_string, min.x, min.y,
             min.z);
      printf("%s      max: %f %f %f\n", indentation_string, max.x, max.y,
             max.z);
      printf("%s      cull_mask: 0x%02x\n", indentation_string,
             child->cull_mask);

      if (child_type == nvk_node_internal) {
         uint32_t child_index = node->relative_child_index + internal_index;

         struct nvk_internal_node *child_node = node + child_index;
         assert(child_index == (uint32_t)-child_node->relative_parent_index);
         assert((child_node->node_info >> 28) == i);

         print_internal(child_node, indentation + 6);

         internal_index++;
      } else if (child_type == nvk_node_instance) {
         struct nvk_instance_node *child_node =
            (struct nvk_instance_node *)((uint8_t *)node +
                                         node->relative_leaf_offset);
         child_node += instance_index;

         print_instance(child_node, indentation + 6);

         instance_index++;
      } else {
         triangle_index++;
      }
   }

   if (triangle_index) {
      struct nvk_triangle_node *child_node =
         (struct nvk_triangle_node *)((uint8_t *)node +
                                      node->relative_leaf_offset);
      print_triangle(child_node, indentation + 3);
   }

   free(indentation_string);
}

static void
test_case(struct bvh_builder *b, struct nvk_internal_node *tmp_internal,
          uint32_t index)
{
   std::vector<nvk_vec3> vertices;

   // for (uint32_t i = 0; i < 1000 * 3; i++)
   // vertices.push_back(
   //    nvk_vec3(random_float(), random_float(), random_float()));

   // for (uint32_t i = 0; i < 10000; i++) {
   //    vertices.push_back(nvk_vec3(i, i, i));
   //    vertices.push_back(nvk_vec3(i + 0.1, i, i));
   //    vertices.push_back(nvk_vec3(i, i + 0.1, i));
   // }

   vertices.push_back(nvk_vec3(0, 1, 0.5));
   vertices.push_back(nvk_vec3(0, 0, 0.5));
   vertices.push_back(nvk_vec3(1, 0, 0.5));

   // vertices.push_back(nvk_vec3(random_float(), random_float(),
   // random_float())); vertices.push_back(nvk_vec3(random_float(),
   // random_float(), random_float()));
   // vertices.push_back(nvk_vec3(random_float(), random_float(),
   // random_float()));

   //  vertices.push_back(nvk_vec3(random_float(),
   //  random_float(), random_float()));
   //  vertices.push_back(nvk_vec3(random_float(), random_float(),
   //  random_float())); vertices.push_back(nvk_vec3(random_float(),
   //  random_float(), random_float())); vertices.push_back(nvk_vec3(4, 0,
   //  0)); vertices.push_back(nvk_vec3(0, 4, 0)); vertices.push_back(nvk_vec3(0,
   //  0, 4)); vertices.push_back(nvk_vec3(2, 0, 0));

   // for (uint32_t i = 0; i < 3; i++)
   //    vertices.push_back(nvk_vec3(0, 0, 0));

   // vertices.push_back(nvk_vec3(1, 0, 0));
   // vertices.push_back(nvk_vec3(0, 1, 0));
   // vertices.push_back(nvk_vec3(0, 0, 1));

   // for (uint32_t i = 0; i < index * 3; i++)
   //    vertices.push_back(nvk_vec3(NAN, NAN, NAN));

   // vertices.push_back(nvk_vec3(2, 0, 0));
   // vertices.push_back(nvk_vec3(1, 1, 0));
   // vertices.push_back(nvk_vec3(1, 0, 1));
   // vertices.push_back(nvk_vec3(0, 1, 0));
   // vertices.push_back(nvk_vec3(0, 0, 1));
   // vertices.push_back(nvk_vec3(0.5, 0, 0));
   // vertices.push_back(nvk_vec3(0, 0.5, 0));
   // vertices.push_back(nvk_vec3(0, 0, 0.5));
   // vertices.push_back(nvk_vec3(0.25, 0, 0));
   // vertices.push_back(nvk_vec3(0, 0.25, 0));
   // vertices.push_back(nvk_vec3(0, 0, 0.25));
   // vertices.push_back(nvk_vec3(0.125, 0, 0));
   // vertices.push_back(nvk_vec3(0, 0.125, 0));
   // vertices.push_back(nvk_vec3(0, 0, 0.125));

   std::vector<VkAccelerationStructureGeometryKHR> geometries;
   std::vector<uint32_t> primitive_counts;

   VkDeviceOrHostAddressConstKHR vertex_data = {
      .hostAddress = vertices.data(),
   };

   std::vector<VkAabbPositionsKHR> aabb_positions;
   aabb_positions.push_back({0.1, 0.1, 0.1, 0.9, 0.9, 0.9});

   VkDeviceOrHostAddressConstKHR aabb_data = {
      .hostAddress = aabb_positions.data(),
   };

   // VkAccelerationStructureGeometryTrianglesDataKHR triangles = {
   //    .sType =
   //       VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_TRIANGLES_DATA_KHR,
   //    .vertexFormat = VK_FORMAT_R32G32B32_SFLOAT,
   //    .vertexData = vertex_data,
   //    .vertexStride = sizeof(struct nvk_vec3),
   //    .maxVertex = (uint32_t)vertices.size() - 1,
   //    .indexType = VK_INDEX_TYPE_NONE_KHR,
   // };

   VkAccelerationStructureGeometryAabbsDataKHR aabbs = {
      .sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_AABBS_DATA_KHR,
      .data = aabb_data,
      .stride = sizeof(VkAabbPositionsKHR),
   };

   VkAccelerationStructureGeometryDataKHR geometry_data = {
      // .triangles = triangles,
      .aabbs = aabbs,
   };

   VkAccelerationStructureGeometryKHR geometry = {
      .sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_KHR,
      // .geometryType = VK_GEOMETRY_TYPE_TRIANGLES_KHR,
      .geometryType = VK_GEOMETRY_TYPE_AABBS_KHR,
      .geometry = geometry_data,
      .flags = 0,
   };

   geometries.push_back(geometry);
   // primitive_counts.push_back(vertices.size() / 3);
   primitive_counts.push_back(aabb_positions.size());

   struct bvh_blueprint blas_blueprint = {
      .geometries = geometries,
      .primitive_counts = primitive_counts,
      .type = VK_ACCELERATION_STRUCTURE_TYPE_BOTTOM_LEVEL_KHR,
   };

   struct bvh blas = bvh_builder_build(b, &blas_blueprint);

   // VkAccelerationStructureBuildGeometryInfoKHR blas_build_info = {
   //    .sType =
   //    VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_GEOMETRY_INFO_KHR, .type
   //    = VK_ACCELERATION_STRUCTURE_TYPE_BOTTOM_LEVEL_KHR, .mode =
   //    VK_BUILD_ACCELERATION_STRUCTURE_MODE_BUILD_KHR, .geometryCount =
   //    (uint32_t)geometries.size(), .pGeometries = geometries.data(),
   // };
   // VkAccelerationStructureBuildRangeInfoKHR blas_range_info = {
   //    .primitiveCount = vertices.size() / 3,
   // };
   // nvk_build_acceleration_structure(blas.buffer.data, &blas_build_info,
   //                                  &blas_range_info);

   assert(blas.buffer.size % 4 == 0);

   struct nvk_bvh_header *prop_header =
      (struct nvk_bvh_header *)blas.buffer.data;

   // assert(prop_header->bvh_size == blas.buffer.size);

   struct nvk_triangle_node *triangle =
      (struct nvk_triangle_node *)((uint8_t *)blas.buffer.data +
                                   prop_header->leaf_nodes_offset);
   // for (uint32_t i = 0; i < 1; i++)
   //    print_triangle(triangle + i);

   // printf("index = %u\n", index + 2);

   struct nvk_internal_node *internal =
      (struct nvk_internal_node *)((uint8_t *)blas.buffer.data +
                                   prop_header->internal_nodes_offset);

   uint8_t *bytes = (uint8_t *)blas.buffer.data;
   // memset(bytes + 0x24, 0, 0x300 - 0x24);
   // memset(bytes + 0x400, 0, 0x200);

   print_internal(internal, 0);

   // for (uint32_t i = 0; i < 64; i++) {
   //    printf("%u: ", i);
   //    print_internal(internal + i);
   // }

   // printf("difference:\n");

   // for (uint32_t i = 0; i < sizeof(struct nvk_triangle_node) / 4; i++) {
   //    uint32_t *src_dwords = (uint32_t *)internal;
   //    uint32_t *dst_dwords = (uint32_t *)tmp_internal;

   //    if (src_dwords[i] != dst_dwords[i]) {
   //       printf("%08x: %08x ", i * 4, src_dwords[i]);
   //       print_bin(src_dwords[i]);
   //       printf(" prev %08x\n", dst_dwords[i]);
   //    }
   // }

   // uint32_t *dwords = (uint32_t *)blas.buffer.data;
   // printf("mystery: ");
   // print_bin(dwords[0x000003fc / 4]);
   // printf("\n");

   // 00100000000000000111011110111101
   // 00110111000000000001100011000110

   // printf("difference:\n");

   // for (uint32_t i = 0; i < sizeof(struct nvk_triangle_node) / 4; i++) {
   //    uint32_t *src_dwords = (uint32_t *)triangle;
   //    uint32_t *dst_dwords = (uint32_t *)tmp_triangle;

   //    if (src_dwords[i] != dst_dwords[i])
   //       printf("%08x: %08x prev %08x\n", i * 4, src_dwords[i],
   //       dst_dwords[i]);
   // }

   // print_triangle(triangle);

   // for (uint32_t i = 0; i < 3 * 8; i++) {
   //    print_bin(triangle->vertex_data[3 * 8 - 1 - i]);
   // }

   // printf("\ndword: ");
   // print_bin(triangle->encoding_control);
   // printf("\nsome number: %u\n", triangle->encoding_control >> 24);
   // printf("float sizes: %u %u %u\n", (triangle->encoding_control >> 10) &
   // 0x1F,
   //        (triangle->encoding_control >> 5) & 0x1F,
   //        (triangle->encoding_control >> 0) & 0x1F);

   // printf("expected v1 bits:\n");
   // print_bin(float_to_bits(vertices[1].x));
   // printf("\n");
   // print_bin(float_to_bits(vertices[1].y));
   // printf("\n");
   // print_bin(float_to_bits(vertices[1].z));
   // printf("\n");

   // printf("\n");

   // struct nvk_vec3 v[NVK_MAX_VERTICES_PER_NODE];
   // nvk_triangle_load_vertices(triangle, v);

   // assert(!memcmp(v, vertices.data(), sizeof(struct nvk_vec3) * 3));

   uint32_t *dwords = (uint32_t *)blas.buffer.data;
   float *floats = (float *)blas.buffer.data;
   for (uint32_t i = 0; i < blas.buffer.size / 4; i++) {
      printf("%08x: %08x ", i * 4, dwords[i]);
      print_bin(dwords[i]);
      printf(" %f", floats[i]);

      if (dwords[i] && dwords[i] % 4 == 0 && dwords[i] < prop_header->bvh_size)
         printf(" // offset");

      if (dwords[i] == vertices.size())
         printf(" // vertex count");

      if (dwords[i] == vertices.size() * 3)
         printf(" // vertex comp count");

      if (dwords[i] == vertices.size() / 3)
         printf(" // primitive count");

      printf("\n");
   }

   // *tmp_triangle = *triangle;
   // *tmp_internal = *internal;

   geometries.clear();
   primitive_counts.clear();

   std::vector<VkAccelerationStructureInstanceKHR> instances;

   for (float x = 0.1; x < 0.9; x += 0.1) {
      for (float y = 0.1; y < 0.9; y += 0.1) {
         VkTransformMatrixKHR transform = {
            .matrix = {{0.1, 0, 0, x}, {0, 0.1, 0, y}, {0, 0, 1, 0}},
         };

         VkAccelerationStructureInstanceKHR instance = {
            .transform = transform,
            .instanceCustomIndex = 0xFFFFFF,
            .mask = 0xFF,
            .instanceShaderBindingTableRecordOffset = 0x123456,
            .flags = index,
            .accelerationStructureReference = bvh_builder_bvh_addr(b, blas),
         };

         instances.push_back(instance);
      }
   }

   VkDeviceOrHostAddressConstKHR instances_addr = {
      .hostAddress = instances.data(),
   };

   VkAccelerationStructureGeometryInstancesDataKHR instances_data = {
      .sType =
         VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_INSTANCES_DATA_KHR,
      .data = instances_addr,
   };

   VkAccelerationStructureGeometryDataKHR instance_geometry_data = {
      .instances = instances_data,
   };

   VkAccelerationStructureGeometryKHR instance_geometry = {
      .sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_KHR,
      .geometryType = VK_GEOMETRY_TYPE_INSTANCES_KHR,
      .geometry = instance_geometry_data,
      .flags = 0,
   };

   geometries.push_back(instance_geometry);
   primitive_counts.push_back(instances.size());

   struct bvh_blueprint tlas_blueprint = {
      .geometries = geometries,
      .primitive_counts = primitive_counts,
      .type = VK_ACCELERATION_STRUCTURE_TYPE_TOP_LEVEL_KHR,
   };

   struct bvh tlas = bvh_builder_build(b, &tlas_blueprint);

   // VkAccelerationStructureBuildGeometryInfoKHR build_info = {
   //    .sType =
   //    VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_GEOMETRY_INFO_KHR, .type
   //    = VK_ACCELERATION_STRUCTURE_TYPE_TOP_LEVEL_KHR, .mode =
   //    VK_BUILD_ACCELERATION_STRUCTURE_MODE_BUILD_KHR, .geometryCount =
   //    (uint32_t)geometries.size(), .pGeometries = geometries.data(),
   // };
   // VkAccelerationStructureBuildRangeInfoKHR range_info = {
   //    .primitiveCount = instances.size(),
   // };
   // nvk_build_acceleration_structure(tlas.buffer.data, &build_info,
   // &range_info);

   bytes = (uint8_t *)tlas.buffer.data;
   // memset(bytes + 0x24, 0, 0x500);
   // memset(bytes + 0x180, 0, 0x00002180 - 0x180);
   // memset(bytes + 0x140, 0, 0x40);
   // memset(bytes + 0x280, 0, 0x100);
   // memset(bytes + 0x28, 0, 0x100);
   // memset(bytes, 0, 12);

   prop_header = (struct nvk_bvh_header *)tlas.buffer.data;

   internal = (struct nvk_internal_node *)((uint8_t *)tlas.buffer.data +
                                           prop_header->internal_nodes_offset);

   // printf("index = %x\n", index);
   // print_internal(internal, 0);

   // bvh_builder_bvh_render(b, tlas, 256, 256);

   // dwords = (uint32_t *)tlas.buffer.data;
   // floats = (float *)tlas.buffer.data;
   // for (uint32_t i = 0; i < tlas.buffer.size / 4; i++) {
   //    printf("%08x: %08x ", i * 4, dwords[i]);
   //    print_bin(dwords[i]);
   //    printf(" %f\n", floats[i]);
   // }

   bvh_builder_bvh_finish(b, tlas);

   bvh_builder_bvh_finish(b, blas);
}

static void
manual_re(struct bvh_builder *b)
{
   std::vector<nvk_vec3> vertices;
   // for (uint32_t i = 0; i < 8; i++) {
   //    vertices.push_back(vec3(i + 0.1, i + 0.2, i + 0.3));
   //    vertices.push_back(vec3(i + 0.1, i + 0.5, i + 0.6));
   //    vertices.push_back(vec3(i + 0.1, i + 0.8, i + 0.9));
   // }
   vertices.push_back(nvk_vec3(1, 0, 0));
   vertices.push_back(nvk_vec3(0, 2.00390625, 0));
   vertices.push_back(nvk_vec3(0, 0, 1));

   std::vector<VkAccelerationStructureGeometryKHR> geometries;
   std::vector<uint32_t> primitive_counts;

   VkDeviceOrHostAddressConstKHR vertex_data = {
      .hostAddress = vertices.data(),
   };

   VkAccelerationStructureGeometryTrianglesDataKHR triangles = {
      .sType =
         VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_TRIANGLES_DATA_KHR,
      .vertexFormat = VK_FORMAT_R32G32B32_SFLOAT,
      .vertexData = vertex_data,
      .vertexStride = sizeof(struct nvk_vec3),
      .maxVertex = (uint32_t)vertices.size() - 1,
      .indexType = VK_INDEX_TYPE_NONE_KHR,
   };

   VkAccelerationStructureGeometryDataKHR geometry_data = {
      .triangles = triangles,
   };

   VkAccelerationStructureGeometryKHR geometry = {
      .sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_KHR,
      .geometryType = VK_GEOMETRY_TYPE_TRIANGLES_KHR,
      .geometry = geometry_data,
      .flags = 1,
   };

   geometries.push_back(geometry);
   primitive_counts.push_back(vertices.size() / 3);

   struct bvh_blueprint blas_blueprint = {
      .geometries = geometries,
      .primitive_counts = primitive_counts,
      .type = VK_ACCELERATION_STRUCTURE_TYPE_BOTTOM_LEVEL_KHR,
   };

   struct bvh blas = bvh_builder_build(b, &blas_blueprint);

   assert(blas.buffer.size % 4 == 0);

   uint32_t *dwords = (uint32_t *)blas.buffer.data;
   float *floats = (float *)blas.buffer.data;

   struct nvk_bvh_header *prop_header =
      (struct nvk_bvh_header *)blas.buffer.data;

   // uint32_t *float_list = (uint32_t *)((uint8_t *)blas.buffer.data +
   //                                     prop_header->float_list_offset);
   // uint32_t float_count =
   //    (prop_header->leaf_nodes_offset - prop_header->float_list_offset) / 4 -
   //    2;
   // for (uint32_t i = 0; i < float_count; i++) {
   //    uint32_t msb = float_list[i] >> 31;
   //    uint32_t lsb = float_list[i] & 1;
   //    printf("%f\n", bits_to_float((float_list[i] & 0x7FFFFFF7) | (lsb << 31)
   //    | msb));
   // }

   // 10111101110011001100101101010010
   // 00111101110011001100110011001101

   struct nvk_bvh_header expected_header = expected_header_base;
   expected_header.bvh_size = blas.buffer.size;

   printf("size: %08x\n", blas.buffer.size);
   for (uint32_t i = 0; i < blas.buffer.size / 4; i++) {
      printf("%08x: %08x %f\n", i * 4, dwords[i], floats[i]);
   }

   printf("\triangle:\n");

   for (uint32_t i = 0; i < sizeof(struct nvk_bvh_header) / 4; i++) {
      uint32_t *src_dwords = (uint32_t *)blas.buffer.data;
      uint32_t *dst_dwords = (uint32_t *)&expected_header;

      if (src_dwords[i] != dst_dwords[i])
         printf("%08x: %08x expected %08x\n", i * 4, src_dwords[i],
                dst_dwords[i]);
   }

   struct nvk_triangle_node *triangle =
      (struct nvk_triangle_node *)((uint8_t *)blas.buffer.data +
                                   prop_header->leaf_nodes_offset);
   print_triangle(triangle, 0);

   printf("mystery: %08x\n", dwords[0x000003fc / 4]);

   // 00100001010001011011000010100100 v20
   // 01100001010001011011000010100100 -v20
   // 11100001010001011011000010100100 v21
   // 11000010100010110110000101001000
   // 01000010100010110110000101001000 ref

   // 00010000001100001100110011001100 v22 2 lsb in v21
   // 01000000110000110011001100110011 ref

   // last dword
   // 00110111000000000001100011000110
   // 00110111000000000001100011001000 // -v00
   // 00110111000000000001100100000110 // -v01
   // 00110111000000000010000011000110 // -v02
   // 00110111000000000001100011001000 // -v10
   // 00110111000000000001100100000110 // -v11
   // 00110111000000000001100011100110 // v11 = 2
   // 00101110000000000011111000001111

   bvh_builder_bvh_finish(b, blas);
}

int
main()
{
   struct bvh_builder b;
   bvh_builder_init(&b);

   char state[32];
   initstate(2938475, state, 32);
   setstate(state);

   struct nvk_internal_node tmp_internal = {};
   for (uint32_t i = 0; i < 1; i++)
      test_case(&b, &tmp_internal, 4);

   // for (uint32_t i = 19; i < 0x00000180 / 4; i++)
   //    printf("   uint32_t unknown%u;\n", i);

   bvh_builder_finish(&b);

   return 0;
}
